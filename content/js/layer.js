function Layer() {
    'use strict';

    var self = this;

/**
 * Exibe Layer de Recuperacao de coffee-pass
 */
this.recuperarCoffePass = function() {
    //TODO: Fazer navegar até a modal aberta
    $('.recovery-coffe-modal').on('click', function() {
        var modal = $(this).attr('data-recovery');

        if (modal === "coffee-pass") {
            $('.modal-perdi-cp').css('top', '-514px');

            $('html, body').stop(true, false).animate({
                 scrollTop: $('.step-coffee-pass').offset().top
             }, 2000);
        } else {
            $('.modal-perdi-cp').css('top', '156px');
            $('html, body').stop(true, false).animate({
                 scrollTop: $('.step-seu-jeito').offset().top
             }, 2000);
        }

        $('.modal-perdi-cp').fadeIn();
    });
};

/**
 * Exibicao da modal cadastro
 */
 this.nextPart = function() {
    var part1 = $('.modal-cadastro .part-1'),
        part2 = $('.modal-cadastro .part-2');

    part1.find('.btn-modal').click(function(){
        part1.addClass('hidden');
        part2.removeClass('hidden');
     });
};

/**
 * Exibicao da modal esqueci senha
 */
 this.recoveryPass = function() {
    $('.pass-recovery').click(function(){
        $('.modal-esqueci-senha').fadeIn();
     });
};

/**
 * Abrir modal - Cadastro
 */
this.openCadastro = function() {
    $('.sign-up').find('.btn').click(function(){
        $('.modal-cadastro').fadeIn();
    });
};

/**
 * Fecha a layer
 */
this.fecharLayer = function() {
    $('.fechar-layer').on('click', function() {
        $(this).parent().fadeOut();
    });
};

/**
 * Valida os temos ao submeter
 */
this.submit = function() {
    $('#finalizar').on('click', function() {
        var termos = $('#fldAcceptTerms').is(':checked');

        if (!termos) {
            $('.layer').fadeOut();
            setTimeout(function(){
                $('.layerTermoServico').fadeIn();
            }, 350);
        }
    });
};


/*
 * Exibe os termos legais de serviço
 */

 this.exibirTermos = function() {

    $('.termos-legais').click('click', function() {
        $('#fldAcceptTerms').attr('checked', 'checked')
        $('.modal-termos').fadeIn();
    });

    $('.link-termos-servicos').on('click', function() {
        $('.layerTermoServico').fadeOut();

        setTimeout(function() {
            $('.modal-termos').fadeIn();
        }, 600);
    });
 };

var receitaAtual;

/**
 * Exibe Modal de deletar minha receita
 */
this.exibeDeletar = function() {

    $('.opcao-deletar').on('click', function() {
        $('.layerConfirmacao').fadeIn();
        receitaAtual = $(this).parents('.coffee-item');
    });
};

/**
 * Cancela deletar modal
 */
this.cancelaDeletar = function() {
    $('.option-modal-cancelar').on('click', function() {
        $('.layerConfirmacao').fadeOut();
    });
};

/**
 * Remove a receita selecionada
 */
this.removeReceita = function() {
    $('.option-modal-delete').on('click', function() {

        $('.layerConfirmacao').fadeOut();

        setTimeout(function() {
            receitaAtual.remove();
        }, 600);
    });
};

 this.init = function() {
    self.fecharLayer();
    self.submit();
    self.recuperarCoffePass();
    self.nextPart();
    self.recoveryPass();
    self.openCadastro();
    self.exibirTermos();
 };

 return self.init();

}
function Slider() {
    'use strict';

    var self = this;

    /*
     * Slider personalizado
     */
     this.customSlider = function() {

        var _slider = $('.slider');

        // cria as barras do slider
        for (var i = 0; i < 100; i++) {
            _slider.find('.slider-bars').append('<li></li>');
        }

        _slider.slider({
            slide: function(event, ui) {

            var LARGURA_BARRA = 8,
                posicao = ui.value * LARGURA_BARRA;

                $(this).find('.mask-slider').css('width', posicao + 'px');

                var nivel = $(this).prev(),
                    persona = $(this).parent().prev();

                self.nivelSlider(ui.value, nivel, persona);
            }
        });
    };

    this.setReceitaSlider = function(persona, intensidade, temperatura) {

        var sliderAtual = persona.attr('data-persona');

        if (sliderAtual === "intensidade") {
            //set intensidade
            receita.setIntensidade(intensidade)
        } else if (sliderAtual === "temperatura") {
            //set temperatura
            receita.setTemperatura(temperatura);
        }
    };

    this.nivelSlider = function(posicao, nivel, persona) {

        var posicaoNivel;

        setTimeout(function() {
            var intensidade = persona.next().find('.active-nivel').text(),
                temperatura = persona.next().find('.status-slide li').eq(intensidade - 1).text();

                self.setReceitaSlider(persona, intensidade, temperatura);
        }, 500);

        // Refatorar
        if (posicao === 0 || posicao <= 15) {
            nivel.find('.numero-nivel').removeClass('active-nivel');
            nivel.find('.numero-nivel').eq(0).addClass('active-nivel');
            persona.css('background-position','center 0');

            posicaoNivel = $('.numero-nivel').eq(0).attr('data-position');

            self.moveStop(nivel.next(), posicaoNivel);
        }

        else if (posicao > 15 && posicao <= 35) {
            nivel.find('.numero-nivel').removeClass('active-nivel');
            nivel.find('.numero-nivel').eq(1).addClass('active-nivel');
            persona.css('background-position','center -307px');

            posicaoNivel = $('.numero-nivel').eq(1).attr('data-position');

            self.moveStop(nivel.next(), posicaoNivel);
        }

        else if (posicao > 35 && posicao <= 65) {
            nivel.find('.numero-nivel').removeClass('active-nivel');
            nivel.find('.numero-nivel').eq(2).addClass('active-nivel');
            persona.css('background-position','center -613px');

            posicaoNivel = $('.numero-nivel').eq(2).attr('data-position');

            self.moveStop(nivel.next(), posicaoNivel);
        }

        else if (posicao > 65 && posicao <= 85) {
            nivel.find('.numero-nivel').removeClass('active-nivel');
            nivel.find('.numero-nivel').eq(3).addClass('active-nivel');
            persona.css('background-position','center -919px');

            posicaoNivel = $('.numero-nivel').eq(3).attr('data-position');

            self.moveStop(nivel.next(), posicaoNivel);
        }

        else if (posicao >= 85) {
            nivel.find('.numero-nivel').removeClass('active-nivel');
            nivel.find('.numero-nivel').eq(4).addClass('active-nivel');
            persona.css('background-position','center -1225px');

            posicaoNivel = $('.numero-nivel').eq(4).attr('data-position');

            self.moveStop(nivel.next(), posicaoNivel);
        }
    };

    this.moveStop = function(_slider, posicaoNivel) {

        _slider.find('.mask-slider').css('width', posicaoNivel * 8 + 'px');

        _slider.slider({
            animate: "slow",
            stop: function() {

                setTimeout(function() {
                    _slider.slider("value", posicaoNivel);
                }, 500);
            }
        });
    };

    this.moverPosicaoSlider = function() {
        $('.numero-nivel').on('click', function() {

            var posicaoNivel = $(this).attr('data-position');

            var _slider = $(this).parents('.nivel-slider').next();
            var mask    = _slider.find('.mask-slider');
            var nivel   = _slider.prev();
            var persona = _slider.parent().prev();


            mask.css('width', posicaoNivel * 8 + 'px');
            _slider.slider( "option", "value", posicaoNivel);

            var posicao = _slider.slider("option", "value");

            self.nivelSlider(posicao, nivel, persona);
        });
    };

    this.init = function() {
        self.customSlider();
        self.moverPosicaoSlider();
    };

    return self.init();
}
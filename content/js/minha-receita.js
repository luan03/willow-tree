function MinhaReceita() {
    'use strict';

    var self = this;

    /**
     * Exibe o menu da sua receita
     */
    this.exibirMenuMinhaReceita = function() {
        var start = true,
            posicao,
            HIDE = -188,
            SHOW = 0;

        $('.show-receita').on('click', function() {

            if (start) {
                posicao = SHOW;
                start = false;
            } else {
                posicao = HIDE;
                start = true;
            }

            $('.sua-receita').stop(false, false).animate({
                left: posicao
            }, 400);
        });
    };

    /**
     * Seleciona café minha receita
     */
    this.selecionarMinhaReceita = function() {
        $('.minha-receita').find('.coffee-select').on('click', function() {

            $('.minha-receita').find('.coffee-select')
                .removeClass('btn-default')
                    .text('Selecionar').attr('title', 'Selecionar');

            $(this).addClass('btn-default')
                .text('Selecionado')
                    .attr('title', 'Selecionado');
        });
    };

    /**
     * Seleciona imagem minha receita
     */
    this.selecionarImagemReceita = function() {
        $('.image-list').find('li').on('click', function() {
            $('.image-list').find('li').removeClass('image-selected');

            $(this).addClass('image-selected');

            var imagem = $(this).find('img').attr('alt') + ".png",
                previewImagem = $('.name-preview').find('img').attr('src').replace(/\w+.\w+$/g, imagem);

            $('.name-preview').find('img').attr('src', previewImagem);
            $('.receita-final').find('img').attr('src', previewImagem);

            // set nome
            receita.setImagem($(this).find('img').attr('alt'));
        });
    };

    /**
     * Favorita café minha receita
     */
    this.favoritarMinhaReceita = function() {
        $('.opcao-favoritar').find('.fa').on('click', function() {

            $('.opcao-favoritar').find('.fa').addClass('fa-star-o');
            $('.opcao-favoritar').find('.fa').removeClass('fa-star');

            $(this).toggleClass('fa-star-o', 'remove');
            $(this).toggleClass('fa-star', 'add');

            var minhaReceita = $(this).parents('.minha-receita');

            $('.minha-receita').removeClass('minha-receita-selected');
            minhaReceita.addClass('minha-receita-selected');
        });
    };

    /**
     * Seta o tamanho do nome inicial da receita
     */
    this.setTamanhoInicial = function() {
        var tamanhoAtual = $('#nomeReceita').val().length;
        $('.status-atual').text(tamanhoAtual);

        self.tamanhoNomeReceita();
    }

    /**
     * Atualiza o tamanho do nome da receita
     */
    this.tamanhoNomeReceita = function() {

        $('#nomeReceita').on('keyup', function() {
            var tamanho = $(this).val().length;
            var MAXIMO = 30;

            if (tamanho > MAXIMO) {
                return false;
            }

            $('.status-atual').text(tamanho);
        });
    };

    /**
     * Atualiza a musica da receita
     */
    this.musicaReceita = function() {

        $('#tituloMusica').on('blur', function() {

            // set musica
            receita.setMusica($('#nomeArtista').val(), $('#tituloMusica').val());
        });

        $('#nomeArtista').on('blur', function() {

            // set musica
            receita.setMusica($('#nomeArtista').val(), $('#tituloMusica').val());
        });
    };

    /**
     * Set nome da receita
     */
    this.nomeReceita = function() {

        $('#nomeReceita').on('blur', function() {

            // set nome
            receita.setNome($(this).val());
        });
    };

    this.init = function() {
        self.exibirMenuMinhaReceita();
        self.selecionarMinhaReceita();
        self.favoritarMinhaReceita();
        self.selecionarImagemReceita();
        self.setTamanhoInicial();
        self.musicaReceita();
        self.nomeReceita();
    };

    return self.init();
}
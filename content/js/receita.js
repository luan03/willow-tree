function Receita() {
    'use strict';

    var receitaAtual = {
        grao: "",
        metodo: "",
        intensidade: "",
        temperatura: "",
        musica: {"artista": '', "titulo": ''},
        nome: "",
        imagem: ""
    };

    this.getReceitaAtual = function() {
        return receitaAtual;
    };

    this.setGrao = function(grao) {
        receitaAtual.grao = grao;
    };

    this.getGrao = function() {
        return receitaAtual.grao;
    };

    this.setMetodo = function(metodo) {
        receitaAtual.metodo = metodo;
    };

    this.getMetodo = function() {
        return receitaAtual.metodo;
    };

    this.setIntensidade = function(intensidade) {
        receitaAtual.intensidade = intensidade;
    };

    this.getIntensidade = function() {
        return receitaAtual.intensidade;
    };

    this.setTemperatura = function(temperatura) {
        receitaAtual.temperatura = temperatura;
    };

    this.getTemperatura = function() {
        return receitaAtual.temperatura;
    };

    this.setMusica = function(artista, titulo) {
        receitaAtual.musica.artista = artista;
        receitaAtual.musica.titulo = titulo;
    };

    this.getMusica = function() {
        return receitaAtual.musica;
    };

    this.setNome = function(nome) {
        receitaAtual.nome = nome;
    };

    this.getNome = function() {
        return receitaAtual.nome;
    };

    this.setImagem = function(imagem) {
        receitaAtual.imagem = imagem;
    };

    this.getImagem = function() {
        return receitaAtual.imagem;
    };
}
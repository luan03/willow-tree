function Produtos() {
    'use strict';

    var self = this;

    /**
     * Ordena os boxs de produtos
     */
     this.ordenarProdutos = function() {
        var $container = $('.vitrine');

        $container.packery({
            itemSelector: '.item',
            gutter: 20
        });
     };

    this.init = function() {
        self.ordenarProdutos();
    };

    return self.init();
}

$(document).ready(function() {
    var produtos = new Produtos();
});
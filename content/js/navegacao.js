function Navegacao() {
    'use strict';

    var self = this;

    /**
     * Cria os bullets da navegacao Lateral
     */
    this.bullets = function() {

        $('.step').each(function(){

            $('.nav-float ul').append(
                '<li data-section=' + $(this).attr('data-step') + '><span class="nomeStep">'+ $(this).attr('name-step') +'</span><span class="valor-atual-nav"></span></li>'
            );

            // Não exibibe mensagem na lateral
            if ($(this).attr('data-step') === "confira" || $(this).attr('data-step') === "desfrute" || $(this).attr('data-step') === "cadastro" || $(this).attr('data-step') === "seu-jeito") {
                $('.nav-float ul li[data-section='+ $(this).attr('data-step') +']').find(".valor-atual-nav").remove();
            }

        });

        self.move();
    };

    /**
     * Faz a navegacao lateral
     */
    this.move = function() {

        var itemNavegacao = $('.nav-float ul li');

        itemNavegacao.on('click', function(){

            var element = $(this).attr('data-section');
            element = $('section.step-' + element);


            $('html, body').stop(true, false).animate({
                 scrollTop: element.offset().top
             }, 2000);
        });
    };

    /**
     * Atualiza a navegacao
     */
    this.scroll = function() {

        $(window).on('scroll', function(){

            atualizar.update();

            var posicao = $(this).scrollTop();
            var ESCOLHA_CAFE = $('.step-escolha-cafe').attr('data-height');

            posicao >= ESCOLHA_CAFE ? $('.nav-float').fadeIn() : $('.nav-float').fadeOut();


            $('section[data-step]').each(function() {
                //TODO: Ultimo item da navegacao

                if (posicao >= $(this).attr('data-height')) {
                    $('.nav-float ul li').removeClass('active-section').find('.nomeStep, .valor-atual-nav').hide();
                    $('.nav-float ul li[data-section='+ $(this).attr('data-step') +']').addClass('active-section').find('.nomeStep, .valor-atual-nav').show();
                }
            });
        });
    };

    /**
     * Seta a posicao de cada secao
     */
    this.setScroll = function() {

        $("section[data-height='']").each(function(){

            var altura = $(this).offset().top;

            $(this).attr("data-height", altura);
        });
    };

    /**
     * Navegacao para o próximo step
     */
    this.navProximo = function() {

        $('.navegacao-central.proximo').on('click', function(){

            var target = $(this).parent().next();

            $('html, body').stop(true, false).animate({
                 scrollTop: target.offset().top
             }, 2000);
        });
    };

    /**
     * Navegacao para o step anterior
     */
    this.navAnterior = function() {

        $('.navegacao-central.anterior').on('click', function(){

            var target = $(this).parent().prev();

            $('html, body').stop(true, false).animate({
                 scrollTop: target.offset().top
             }, 2000);
        });
    };

    this.init = function() {
        self.scroll();
        self.setScroll();
        self.bullets();
        self.navProximo();
        self.navAnterior();
    };

    return self.init();
};
/**
 * Atualiza a Receita atual.
 * @class AtualizarReceita
 */
function AtualizarReceita() {
    'use strict';

    var self = this;

    /**
     * Atualiza o tipo de grão
     * @method grao
     */
    this.grao = function() {

        if (receita.getGrao() === "") {
            return false;
        }

        $('.grao-option').text(receita.getGrao() + ".");
        $('.nav-float ul li[data-section="selecione-grao"]').find('.valor-atual-nav').text(receita.getGrao() + ".");
    };

    /**
     * Atualiza o método de extração
     * @method metodo
     */
    this.metodo = function() {

        if (receita.getMetodo() === "") {
            return false;
        }

        $('.metodo-option').text(receita.getMetodo() + ".");
        $('.nav-float ul li[data-section="metodo-extracao"]').find('.valor-atual-nav').text(receita.getMetodo() + ".");
    };

    /**
     * Atualiza a intensidade do café
     * @method intensidade
     */
    this.intensidade = function() {

        if (receita.getIntensidade() === "") {
            return false;
        }

        $('.intensidade-option').text(receita.getIntensidade() + ".");
        $('.nav-float ul li[data-section="intensidade"]').find('.valor-atual-nav').text(receita.getIntensidade() + ".");
    };

    /**
     * Atualiza a temperatura do café
     * @method temperatura
     */
    this.temperatura = function() {

        if (receita.getTemperatura() === "") {
            return false;
        }

        $('.temperatura-option').text(receita.getTemperatura() + ".");
        $('.nav-float ul li[data-section="temperatura"]').find('.valor-atual-nav').text(receita.getTemperatura() + ".");
    }

    /**
     * Atualiza a musica para ouvir
     * @method musica
     */
    this.musica = function() {

        if (receita.getMusica()["artista"] === "" && receita.getMusica()["titulo"] === "") {
            return false;
        }

        var musicaObject = receita.getMusica();

        $('.artista-option').text(musicaObject["artista"]);
        $('.titulo-option').text(musicaObject["titulo"]);

        $('.nav-float ul li[data-section="musica"]').find('.valor-atual-nav').text(musicaObject["artista"] + " - " +  musicaObject["titulo"]);

    };

    /**
     * Atualiza o nome da receita
     * @method nome
     */
    this.nome = function() {

        if (receita.getNome() === "") {
            return false;
        }

        $('.nome-option').text(receita.getNome());
        $('.nav-float ul li[data-section="nome-receita"]').find('.valor-atual-nav').text(receita.getNome() + ".");
    };

    /**
     * Atualiza a imagem selecionada
     * @method imagem
     */
    this.imagem = function() {

         if (receita.getImagem() === "") {
            return false;
        }

        $('.nav-float ul li[data-section="imagem-receita"]').find('.valor-atual-nav').text(receita.getImagem() + ".");
    };

    /**
     * Atualiza a receita atual
     * @method imagem
     */
    this.update = function() {
        self.grao();
        self.metodo();
        self.intensidade();
        self.temperatura();
        self.musica();
        self.nome();
        self.imagem();
    };
}

var atualizar = new AtualizarReceita();
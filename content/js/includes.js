var events = new Events(),
    layer = new Layer();

$(function(){

    // INCLUDE - CADASTRO
    $('.inc-cadastro').load('../includes/modal/cadastro.html', function() {
        layer.fecharLayer();
        layer.nextPart();
        layer.recoveryPass();
    });

    // INCLUDE - ESQUECI SENHA
    $('.inc-esqueci-senha').load('../includes/modal/esqueci-senha.html', function() {
        layer.fecharLayer();
    });

    // INCLUDE - PERDI MEU COFFEE PASS
    $('.inc-perdi-cf').load('../includes/modal/perdi-coffee-pass.html', function() {
        layer.fecharLayer();
    });

    // INCLUDE - TERMOS
    $('.inc-termos').load('../includes/modal/termos.html', function() {
        events.barraRolagem();
        layer.fecharLayer();
    });

    // INCLUDE - CONFORMAÇÃO
    $('.inc-confirmacao').load('../includes/modal/confirmacao.html', function() {
        layer.fecharLayer();
        layer.cancelaDeletar();
        layer.removeReceita();
        layer.exibeDeletar();
    });

});
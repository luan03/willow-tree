function Events() {
    'use strict';

    var self = this;

    /**
     * Personaliza os selects das paginas
     */
    this.select = function() {

        $('.select-form select').on('change', function() {
            var MAX_STRING = 18,
                option = $(this).val();

            if (option.length > MAX_STRING) {
                option = option.substring(0, MAX_STRING);
            }

            $(this).prev().text(option);
        });
    };

    /**
     * Hover dos Helps das secoes
     */
    this.hover = function() {
        $('.fa-question-circle').hover(
            function() {
                $(this).find('.helps').fadeIn();
            }, function() {
                $(this).find('.helps').fadeOut();
            }
        );
    };

    /**
     * Favorita cafe pré-selecionado para o usuário
     */
    this.favoritarCafeDefault = function() {
        $('.item-default').not('.item-recipe').find('.coffee-select').on('click', function() {

            $('.item-default').not('.item-recipe').removeClass('coffee-selected');
            $(this).parents('.item-default').not('.item-recipe').addClass('coffee-selected');

            $('.item-default').not('.item-recipe').find('.coffee-select')
                .removeClass('btn-default')
                    .text('Favoritar').attr('title', 'Favoritar');

            $(this).addClass('btn-default')
                .text('Favorito')
                    .attr('title', 'Favorito');
        });
    };

    /**
     * Seleciona grao para o usuário
     */
    this.selecionarOpcaoGrao = function() {
        $('.grao-method').find('.coffee-select').on('click', function() {

            // set grao
            receita.setGrao($(this).parent().prev().attr('title'));

            $(this).parents('.recipe-coffee').find('.coffee-item').removeClass('coffee-selected');
            $(this).parents('.item-recipe').addClass('coffee-selected');

            $(this).parents('.recipe-coffee').find('.coffee-select')
                .removeClass('btn-default')
                    .text('Selecionar').attr('title', 'Selecionar');

            $(this).addClass('btn-default')
                .text('Selecionado')
                    .attr('title', 'Selecionado');
        });
    };

    /**
     * Seleciona método de extração para o usuário
     */
    this.selecionarOpcaoMetodo = function() {
        $('.coffee-method').find('.coffee-select').on('click', function() {

            // set metodo
            receita.setMetodo($(this).parent().prev().attr('title'));

            $(this).parents('.recipe-coffee').find('.coffee-item').removeClass('coffee-selected');
            $(this).parents('.item-recipe').addClass('coffee-selected');

            $(this).parents('.recipe-coffee').find('.coffee-select')
                .removeClass('btn-default')
                    .text('Selecionar').attr('title', 'Selecionar');

            $(this).addClass('btn-default')
                .text('Selecionado')
                    .attr('title', 'Selecionado');
        });
    };

     /*
     * Barra de rolagem personalizada
     */

     this.barraRolagem = function() {
        $('.scroll-pane').jScrollPane({
            verticalDragMinHeight: 33,
            verticalDragMaxHeight: 33,
            autoReinitialise: true
        });
     };

     this.init = function(){

        var navegacao    = new Navegacao(),
            layer        = new Layer(),
            minhaReceita = new MinhaReceita(),
            slider       = new Slider();

        self.select();
        self.hover();
        self.favoritarCafeDefault();
        self.barraRolagem();
        self.selecionarOpcaoGrao();
        self.selecionarOpcaoMetodo();
    };
}

$(document).ready(function() {
    var events = new Events();
    events.init();
});
